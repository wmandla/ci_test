from PyQt5 import QtCore

from view.MainWindow import MainWindow


def test_hello(qtbot):
    window = MainWindow()
    qtbot.addWidget(window)

    expectedText = "Hello World!"
    window.ui.lineEdit.setText(expectedText)
    # click on the button and make sure it updates the appropriate label
    qtbot.mouseClick(window.ui.pushButton, QtCore.Qt.LeftButton)

    assert window.ui.label.text() == expectedText
