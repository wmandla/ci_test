from PyQt5 import QtWidgets, uic

from view.repeater import Ui_MainWindow


class Ui(QtWidgets.QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()
        uic.loadUi('basic.ui', self)
        self.show()


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent=parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.pushButton.clicked.connect(self.my_function)

    def my_function(self):
        my_text = self.ui.lineEdit.text()
        self.ui.label.setText(my_text)
