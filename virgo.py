import sys
from PyQt5 import QtWidgets

from view.MainWindow import MainWindow


def functionA():
    print("print virgo")
    return 3


def func(a: int, b: int) -> str:
    return str(a + b)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
